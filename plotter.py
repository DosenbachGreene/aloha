#!/usr/bin/env python3

import csv
import matplotlib.pyplot as plt
import numpy

threshold = 10
res = "subject" # resolution
xaxis = "Age"
yaxis = "MAUI"
#zaxis = "UseRatio"

# open csv
BPEAC = []
hemiBPEAC = []
CIMT = []
with open('./output/BPEAC_{}_threshold_{}.csv'.format(res,threshold),'r',newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        BPEAC.append(row)
with open('./output/hemiBPEAC_{}_threshold_{}.csv'.format(res,threshold),'r',newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        hemiBPEAC.append(row)
with open('./output/CIMT_{}_threshold_{}.csv'.format(res,threshold),'r',newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        CIMT.append(row)

# subs to leave in data
#leavein = ['BPEAC259H','CIMT010T','BPEAC175H','CIMT001T','BPEAC222H','CIMT002T','CIMT021TR',
#    'BPEAC056H','CIMT011T','BPEAC086H','CIMT009T','BPEAC087H','CIMT008T','BPEAC007H','CIMT004T','CIMT024T']
leavein = []
BPEAC = [sub for sub in BPEAC if sub['Subject'] not in leavein]
hemiBPEAC = [sub for sub in hemiBPEAC if sub['Subject'] not in leavein]
CIMT = [sub for sub in CIMT if sub['Subject'] not in leavein]

# create a numpy array out of the data
BPEAC_x = numpy.asarray([visit[xaxis] for visit in BPEAC],dtype=float)
BPEAC_y = numpy.asarray([visit[yaxis] for visit in BPEAC],dtype=float)
#BPEAC_z = numpy.asarray([visit[zaxis] for visit in BPEAC],dtype=float)
hemiBPEAC_x = numpy.asarray([visit[xaxis] for visit in hemiBPEAC],dtype=float)
hemiBPEAC_y = numpy.asarray([visit[yaxis] for visit in hemiBPEAC],dtype=float)
#hemiBPEAC_z = numpy.asarray([visit[zaxis] for visit in hemiBPEAC],dtype=float)
CIMT_x = numpy.asarray([visit[xaxis] for visit in CIMT],dtype=float)
CIMT_y = numpy.asarray([visit[yaxis] for visit in CIMT],dtype=float)
#CIMT_z = numpy.asarray([visit[zaxis] for visit in CIMT],dtype=float)
hemi_x = numpy.concatenate((hemiBPEAC_x,CIMT_x))
hemi_y = numpy.concatenate((hemiBPEAC_y,CIMT_y))
#hemi_z = numpy.concatenate((hemiBPEAC_z,CIMT_z))

# create figure for plotting
fig,ax = plt.subplots(figsize=(10,6))
ax.scatter(BPEAC_x,BPEAC_y,c='green',label='BPEAC',alpha=0.8, edgecolors='none')
ax.scatter(hemi_x,hemi_y,c='red',label='hemi',alpha=0.8, edgecolors='none')
ax.set_xlim([0,18])
ax.set_xlabel(xaxis)
ax.set_ylim([0,2])
ax.set_ylabel(yaxis)
ax.legend(loc=1)
#for csub,cx,cy in zip(BPEAC,BPEAC_x,BPEAC_y):
#    ax.annotate(csub['Subject'],xy=(float(cx),float(cy)),size=6)
for hsub,hx,hy in zip(numpy.concatenate((hemiBPEAC,CIMT)),hemi_x,hemi_y):
    ax.annotate(hsub['Subject'],xy=(float(hx),float(hy)),size=6)
plt.show()
