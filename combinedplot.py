#!/usr/bin/env python3
import numpy
import pickle
import matplotlib.pyplot as plt

with open('BPEAC199C_bi.pkl','rb') as f:
    Bbi = pickle.load(f)
with open('BPEAC199C_uni.pkl','rb') as f:
    Buni = pickle.load(f)
with open('CIMT005T_bi.pkl','rb') as f:
    Cbi = pickle.load(f)
with open('CIMT005T_uni.pkl','rb') as f:
    Cuni = pickle.load(f)

# make figure
fig = plt.figure(figsize=(18,12))

ax = fig.add_subplot(2,2,1)
left = Buni[0]
right = Buni[1]
ax.hist(left[numpy.nonzero(left)],bins=range(-1000,0,10),facecolor='b',alpha=0.75,label="Left")
ax.hist(right[numpy.nonzero(right)],bins=range(0,1000,10),facecolor='r',alpha=0.75,label="Right")
ax.set_xlabel('Acceleration of Activity (1 count = 0.0163 m/s^2)')
ax.set_ylabel('Frequency')
ax.set_xlim(-600,600)
ax.set_ylim(0,3000)
ax.set_xticklabels(['600','400','200','0','200','400','600'])
ax.text(400,2500,'0.95',backgroundcolor='#A3DEB5',fontsize=26)
ax.text(-750,3100,'A',color='#35832E',fontsize=20)
ax.text(-875,2500,'Typical; 8 y/o F',rotation='vertical',color='#35832E',fontsize=20)
ax.set_title('MAUI',fontsize=26)
ax.legend(loc='upper left')

ax = fig.add_subplot(2,2,2)
left = Bbi[0]
right = Bbi[1]
ax.hist(left[numpy.nonzero(left)],bins=range(-1000,0,10),facecolor='b',alpha=0.75)
ax.hist(right[numpy.nonzero(right)],bins=range(0,1000,10),facecolor='r',alpha=0.75)
ax.set_xlabel('Acceleration of Activity (1 count = 0.0163 m/s^2)')
ax.set_ylabel('Frequency')
ax.set_xlim(-600,600)
ax.set_ylim(0,3000)
ax.set_xticklabels(['600','400','200','0','200','400','600'])
ax.text(400,2500,'1.06',backgroundcolor='#A3DEB5',fontsize=26)
ax.set_title('BAUI',fontsize=26)
ax.text(-750,3100,'B',color='#35832E',fontsize=20)

ax = fig.add_subplot(2,2,3)
left = Cuni[0]
right = Cuni[1]
ax.hist(left[numpy.nonzero(left)],bins=range(-1000,0,10),facecolor='b',alpha=0.75)
ax.hist(right[numpy.nonzero(right)],bins=range(0,1000,10),facecolor='r',alpha=0.75)
ax.set_xlabel('Acceleration of Activity (1 count = 0.0163 m/s^2)')
ax.set_ylabel('Frequency')
ax.set_xlim(-600,600)
ax.set_ylim(0,3000)
ax.set_xticklabels(['600','400','200','0','200','400','600'])
ax.text(400,2500,'0.10',backgroundcolor='#F9C8B2',fontsize=26)
ax.text(-750,3100,'C',color='#D86E33',fontsize=20)
ax.text(-875,2700,'Asymmetrical; 8 y/o F',rotation='vertical',color='#D86E33',fontsize=20)

ax = fig.add_subplot(2,2,4)
left = Cbi[0]
right = Cbi[1]
ax.hist(left[numpy.nonzero(left)],bins=range(-1000,0,10),facecolor='b',alpha=0.75)
ax.hist(right[numpy.nonzero(right)],bins=range(0,1000,10),facecolor='r',alpha=0.75)
ax.set_xlabel('Acceleration of Activity (1 count = 0.0163 m/s^2)')
ax.set_ylabel('Frequency')
ax.set_xlim(-600,600)
ax.set_ylim(0,3000)
ax.set_xticklabels(['600','400','200','0','200','400','600'])
ax.text(400,2500,'0.67',backgroundcolor='#F9C8B2',fontsize=26)
ax.text(-750,3100,'D',color='#D86E33',fontsize=20)

plt.savefig('combined_plot.eps')
plt.show()
