#!/usr/bin/env python3

import os
import csv
import glob
import numpy
import argparse
import time
import pickle
from tables import *

class Raw(IsDescription):
    """
        Class for Raw table
    """
    leftx = UInt16Col()
    lefty = UInt16Col()
    leftz = UInt16Col()
    rightx = UInt16Col()
    righty = UInt16Col()
    rightz = UInt16Col()

class Database:
    """
        Database class for storing the dataset
    """

    def __init__(self,dbname):
        # initialize the database object

        # store the database name
        self.dbname = dbname

        # create lists for csv output
        self.visitlist = []
        self.subjectlist = []

        # create dict for variation output
        self.varlist = []

        # check to see of a h5 file already exists
        if os.path.isfile("{}.h5".format(dbname)):
            self.database = open_file('{}.h5'.format(dbname), mode="r+",
                title="{} Database".format(os.path.basename(dbname))) # open the database
        else:
            self.database = open_file('{}.h5'.format(dbname), mode="w",
                title="{} Database".format(os.path.basename(dbname))) # create a new database

    def close(self):
        # Close database object
        self.database.close()

    def __str__(self):
        return str(self.database)

    def subject(self,name,age=-1,domhand='',rtn_node=False):
        # Return/Create subject in database

        try: # try to create a new subject group
            subject_group = self.database.create_group(
                "/", "{}".format(name),
                "{} data".format(name)
            )
            # Set attributes
            self.database.set_node_attr("/{}".format(name),'Domhand',domhand)
            self.database.set_node_attr("/{}".format(name),'Age',age)
            exists = False # set exists flag
        except NodeError: # if already exists just return the subject node
            subject_group = self.database.get_node("/{}".format(name))
            exists = True # set exists flag

        # return the group object if true, otherwise just return the name string
        if rtn_node:
            return subject_group,exists
        else:
            return subject_group._v_name,exists

    def visit(self,subject,visit,date='',rtn_node=False):
        # Return/Create visit in database

        try: # try to create a new visit group
            visit_group = self.database.create_group(
                "/{}".format(subject),
                "{}".format(visit),
                "{} data".format(visit)
            )
            # Set attributes
            self.database.set_node_attr("/{}/{}".format(subject,visit),'Date',date)
            exists = False # set exists flag
        except NodeError: # if already exists just return the visit node
            visit_group = self.database.get_node("/{}/{}".format(subject,visit))
            exists = True # set exists flag

        # return the group object if true, otherwise just return the name string
        if rtn_node:
            return visit_group,exists
        else:
            return visit_group._v_name,exists

    def rawdata(self,subject,visit,rawobj=None,rtn_node=False):
        # Create/Set/Get the rawdata for the subject/visit
        try: # try to create a new raw data table
            raw_table = self.database.create_table(
                "/{}/{}".format(subject,visit),
                "raw",
                Raw,
                "Raw Data",
                expectedrows=100000
            )
            exists = False # set exists flag
        except NodeError: # if already exists just return the raw table node
            raw_table = self.database.get_node("/{}/{}/raw".format(subject,visit))
            exists = True # set exists flag

        # Populate the visit with data if the csv object is defined
        if rawobj != None and (not exists):
            # do each side
            for side in ['Left','Right']:
                # Set attributes
                self.database.set_node_attr("/{}/{}".format(subject,visit),
                    '{}SerialNumber'.format(side),rawobj.raw[side]['SerialNumber'])
                self.database.set_node_attr("/{}/{}".format(subject,visit),
                    '{}StartTime'.format(side),rawobj.raw[side]['StartTime'])
                self.database.set_node_attr("/{}/{}".format(subject,visit),
                    '{}StartDate'.format(side),rawobj.raw[side]['StartDate'])
                self.database.set_node_attr("/{}/{}".format(subject,visit),
                    '{}EpochPeriod'.format(side),rawobj.raw[side]['EpochPeriod'])

            # delete the table before writing
            raw_table.remove_rows(start=0)

            # loop through the data
            for leftepoch,rightepoch in zip(rawobj.raw['Left']['data'],rawobj.raw['Right']['data']):
                # get row
                tablerow = raw_table.row
                # populate row
                tablerow['leftx'] = leftepoch[0]
                tablerow['lefty'] = leftepoch[1]
                tablerow['leftz'] = leftepoch[2]
                tablerow['rightx'] = rightepoch[0]
                tablerow['righty'] = rightepoch[1]
                tablerow['rightz'] = rightepoch[2]
                # append row
                tablerow.append()

            # flush the table's I/O buffer
            raw_table.flush()

        # return the group object if true, otherwise just return the name string
        if rtn_node:
            return raw_table,exists
        else:
            return raw_table._v_name,exists

    def process(self,settings):
        # Traverse each subject
        subjects = [subject for subject in self.database.walk_groups() if subject._v_depth == 1]
        for subject in subjects:
            # Check if specific subject defined
            if settings['subject'] != None:
                if subject._v_name != settings['subject']:
                    continue
            print(subject)

            # Create lists to store resultant arrays
            visitleftresults = []
            visitrightresults = []

            # visit epoch list
            visitepoch = []

            # create list to store visit features
            visitMAUI = []

            # get dominant hand
            domhand = self.database.get_node_attr(subject,'Domhand')

            # Traverse each visit of the subject
            visits = [visit for visit in subject._f_walk_groups() if visit._v_depth == 2]
            for visit in visits:
                print(visit)
                # create numpy array
                accelarray = numpy.empty([visit.raw.nrows,6],dtype=numpy.dtype('u2'))

                # Get the raw data to process
                visit.raw.read(out=accelarray)

                # convert to float
                accelarray = accelarray.astype(float)

                # apply cutoff
                cutoff = settings['cutoff']*60
                accelarray = accelarray[cutoff:-cutoff,:]

                # calculate resultant
                leftresultant = numpy.linalg.norm(accelarray[:,0:3],axis=1)
                rightresultant = numpy.linalg.norm(accelarray[:,3:6],axis=1)

                # apply activity threshold
                leftresultant = leftresultant * (leftresultant > settings['threshold'])
                rightresultant = rightresultant * (rightresultant > settings['threshold'])

                # append the resultants to list
                visitleftresults.append(leftresultant)
                visitrightresults.append(rightresultant)

                # print get the epoch period
                epoch = time.strptime(self.database.get_node_attr(visit,'LeftEpochPeriod'),'%H:%M:%S')[5]
                visitepoch.append(epoch)

                # skip if subjects only
                if not settings['subjects_only']:
                    # calculate features from resultants and append to list
                    features = self.__calcfeatures(subject,visit,self.database.get_node_attr(subject,'Age'),
                        leftresultant,rightresultant,domhand,epoch,settings)
                    self.visitlist.append(features)
                    visitMAUI.append(features['MAUI'])

            # concatenate the resultants into one large numpy array
            subleftresultant = numpy.concatenate(visitleftresults)
            subrightresultant = numpy.concatenate(visitrightresults)

            # calculate features from resultants and append to list
            features = self.__calcfeatures(subject,'all',self.database.get_node_attr(subject,'Age'),
                subleftresultant,subrightresultant,domhand,visitepoch[0],settings)
            features['varMAUI'] = numpy.var(visitMAUI) # calculate variance of MAUI across visits
            self.subjectlist.append(features)

    def __calcfeatures(self,subject,visit,age,leftresultant,rightresultant,domhand,epoch,settings):
        # initialize dictionary
        features = {}

        # specify subject, visit
        features['Subject'] = subject._v_name
        # only specify if not subjectwise processing
        if visit != 'all':
            features['Visit'] = visit._v_name

        # specify age
        features['Age'] = age

        # specify dominant hand
        features['Domhand'] = domhand

        # get recording period
        features['Period'] = leftresultant.shape[0]*epoch/60

        # calculate % movement
        features['PercentMovement'] = (numpy.sum((leftresultant != 0)|(rightresultant != 0))/leftresultant.shape[0])*100

        # calculate use counts
        features['LeftUseCount'] = leftresultant[leftresultant != 0].shape[0]
        features['RightUseCount'] = rightresultant[rightresultant != 0].shape[0]

        # calculate unilateral use counts
        features['LeftUniUseCount'] = leftresultant[(leftresultant != 0)*(rightresultant == 0)].shape[0]
        features['RightUniUseCount'] = rightresultant[(rightresultant != 0)*(leftresultant == 0)].shape[0]

        # calculate total activity counts
        features['LeftTAC'] = numpy.sum(leftresultant)
        features['RightTAC'] = numpy.sum(rightresultant)

        # calculate unilateral total activity counts
        features['LeftUTAC'] = numpy.sum(leftresultant[rightresultant == 0])
        features['RightUTAC'] = numpy.sum(rightresultant[leftresultant == 0])

        # calculate bilateral total activity counts
        features['LeftBTAC'] = numpy.sum(leftresultant[rightresultant != 0])
        features['RightBTAC'] = numpy.sum(rightresultant[leftresultant != 0])

        # only plot if setting is enabled
        if settings['plot']:
            # plot unilateral histogram
            fig = plt.figure(figsize=(9,6))
            left = -leftresultant[rightresultant == 0]
            right = rightresultant[leftresultant == 0]
            with open('{}_uni.pkl'.format(subject._v_name),'wb') as f:
                pickle.dump([left,right],f)
            plt.hist(left[numpy.nonzero(left)],bins=range(-1000,0,10),facecolor='b',alpha=0.75)
            plt.hist(right[numpy.nonzero(right)],bins=range(0,1000,10),facecolor='r',alpha=0.75)
            plt.xlabel('Acceleration of Activity (1 count = 0.0163 m/s^2)')
            plt.ylabel('Frequency')
            plt.xlim(-600,600)
            plt.ylim(0,3000)
            plt.axes().set_xticklabels(['600','400','200','0','200','400','600'])
            if visit == 'all':
                plt.title('Activity Distribution of Unilateral Movements for Subject {}'.format(subject._v_name))
                plt.savefig(os.path.join(settings['output'],'{}_unilateral.eps'.format(subject._v_name)))
            else:
                plt.title('Activity Distribution of Unilateral Movements for Subject {}, Visit {}'.format(subject._v_name,visit._v_name))
                plt.savefig(os.path.join(settings['output'],'{}_{}_unilateral.eps'.format(subject._v_name,visit._v_name)))
            plt.close(fig)

            # # plot bilateral histogram
            fig = plt.figure(figsize=(9,6))
            left = -leftresultant[rightresultant != 0]
            right = rightresultant[leftresultant != 0]
            # with open('{}_bi.pkl'.format(subject._v_name),'wb') as f:
            #     pickle.dump([left,right],f)
            plt.hist(left[numpy.nonzero(left)],bins=range(-1000,0,10),facecolor='b',alpha=0.75)
            plt.hist(right[numpy.nonzero(right)],bins=range(0,1000,10),facecolor='r',alpha=0.75)
            plt.xlabel('Acceleration of Activity (1 count = 0.0163 m/s^2)')
            plt.ylabel('Frequency')
            plt.xlim(-600,600)
            plt.ylim(0,3000)
            plt.axes().set_xticklabels(['600','400','200','0','200','400','600'])
            if visit == 'all':
                plt.title('Activity Distribution of Bilateral Movements for Subject {}'.format(subject._v_name))
                plt.savefig(os.path.join(settings['output'],'{}_bilateral.eps'.format(subject._v_name)))
            else:
                plt.title('Activity Distribution of Bilateral Movements for Subject {}, Visit {}'.format(subject._v_name,visit._v_name))
                plt.savefig(os.path.join(settings['output'],'{}_{}_bilateral.eps'.format(subject._v_name,visit._v_name)))
            plt.close(fig)
            # fig = plt.figure(figsize=(10,6))
            # left = leftresultant[rightresultant != 0]
            # right = rightresultant[leftresultant != 0]
            # left_n = numpy.histogram(left[numpy.nonzero(left)],bins=100,range=(0,1000)) # get only nonzero elements
            # right_n = numpy.histogram(right[numpy.nonzero(right)],bins=100,range=(0,1000)) # get only nonzero elements
            # left_power = numpy.repeat(numpy.expand_dims(left_n[0],axis=1),800,axis=1)
            # gap = numpy.repeat(numpy.expand_dims(numpy.zeros(left_n[0].shape[0]),axis=1),20,axis=1)
            # right_power = numpy.repeat(numpy.expand_dims(right_n[0],axis=1),800,axis=1)
            # power = numpy.concatenate((left_power,gap,right_power),axis=1)
            # cplot = plt.contourf(numpy.arange(1620),left_n[1][0:-1],power,100,cmap="hot_r",levels=range(0,10000,100))
            # plt.ylabel('Activity Intensity'); ax = plt.gca(); ax.set_xticklabels([]);
            # ax_c = fig.colorbar(cplot); ax_c.set_label("Samples"); plt.clim(0,10000)
            # if visit == 'all':
            #     plt.title('Activity Distribution of Bilateral Movements for Subject {}'.format(subject._v_name))
            #     plt.savefig(os.path.join(settings['output'],'{}_bilateral.eps'.format(subject._v_name)))
            # else:
            #     plt.title('Activity Distribution of Bilateral Movements for Subject {}, Visit {}'.format(subject._v_name,visit._v_name))
            #     plt.savefig(os.path.join(settings['output'],'{}_{}_bilateral.eps'.format(subject._v_name,visit._v_name)))
            # plt.close(fig)

        # calculate use ratio/activity ratio
        try:
            if domhand == 'RH':
                features['UseRatio'] = features['LeftUseCount']/features['RightUseCount']
                features['UniUseRatio'] = features['LeftUniUseCount']/features['RightUniUseCount']
                features['ActivityRatio'] = features['LeftTAC']/features['RightTAC']
                features['MAUI'] = features['LeftUTAC']/features['RightUTAC']
                features['BAUI'] = features['LeftBTAC']/features['RightBTAC']
            else:
                features['UseRatio'] = features['RightUseCount']/features['LeftUseCount']
                features['UniUseRatio'] = features['RightUniUseCount']/features['LeftUniUseCount']
                features['ActivityRatio'] = features['RightTAC']/features['LeftTAC']
                features['MAUI'] = features['RightUTAC']/features['LeftUTAC']
                features['BAUI'] = features['RightBTAC']/features['LeftBTAC']
        except ZeroDivisionError:
            print("Skipping since not enough samples...")

        # return features
        return features

    def writecsv(self,settings):
        # Write out csv to location

        # open file for visit CSV
        if not settings['subjects_only']:
            with open(os.path.join(settings['output'],'{}_visit_threshold_{}.csv'.format(self.dbname,settings['threshold'])),'w',newline='') as csvfile:
                # create writer object
                writer = csv.DictWriter(csvfile,fieldnames=list(self.visitlist[0].keys()))

                # write header
                writer.writeheader()

                # write out each nrows
                for row in self.visitlist:
                    writer.writerow(row)

        # open file for subject CSV
        with open(os.path.join(settings['output'],'{}_subject_threshold_{}.csv'.format(self.dbname,settings['threshold'])),'w',newline='') as csvfile:
            # create writer object
            writer = csv.DictWriter(csvfile,fieldnames=list(self.subjectlist[0].keys()))

            # write header
            writer.writeheader()

            # write out each nrows
            for row in self.subjectlist:
                writer.writerow(row)

class CSVdata:
    """
        Class representation of raw csv data files
    """

    def __init__(self,leftfile,rightfile):
        # create dictionary
        self.raw = {'Left': {}, 'Right': {}}

        # load csv files
        self.loadcsv(leftfile,'Left')
        self.loadcsv(rightfile,'Right')

    def loadcsv(self,rawfile,side):
        # load csv files into object
        with open(rawfile,'r') as rawcsv:
            # create csv reader
            rawreader = csv.reader(rawcsv,delimiter=',')
            # skip 1st line
            next(rawreader)
            # parse the serial number
            self.raw[side]['SerialNumber'] = next(rawreader)[0].split(': ')[1]
            # parse the start time/date
            self.raw[side]['StartTime'] = next(rawreader)[0].split('Time ')[1]
            self.raw[side]['StartDate'] = next(rawreader)[0].split('Date ')[1]
            # parse the epoch period
            self.raw[side]['EpochPeriod']  = next(rawreader)[0].split('(hh:mm:ss) ')[1]
            # skip next 5 lines
            for i in range(5):
                next(rawreader)
            # read in the actual accelerometry data
            self.raw[side]['data'] = []
            for row in rawreader:
                self.raw[side]['data'].append((int(row[0]),int(row[1]),int(row[2])))

def scancsvfiles(location):
    # Scan all csv files at location
    csvlocations = glob.glob(os.path.join(location,'*.csv'))

    # parse out the file names for each csv
    parsedfiles = [[filename]+os.path.basename(filename).split('_') for filename in sorted(csvlocations)]

    # separate left and right
    leftcsv = [parsed[0:6] for parsed in parsedfiles if parsed[4] == 'L']
    rightcsv = [parsed[0:6] for parsed in parsedfiles if parsed[4] == 'R']

    # assert that both lists are the same
    assert [f[1] for f in leftcsv] == [f[1] for f in rightcsv]
    assert [f[2] for f in leftcsv] == [f[2] for f in rightcsv]

    # concatenate the 2 lists sideways
    csvlist = [f for f in zip(leftcsv,rightcsv)]

    # return the list
    return csvlist

def readagefile(location):
    # open age file for reading
    with open(location,'r') as agefile:
        reader = csv.reader(agefile,delimiter=',')
        # skip first line
        next(reader)

        # store subject and age in dict
        agedict = {}
        for row in reader:
            agedict[row[0]] = float(row[1])

        # return the age dictionary
        return agedict

def builddatabase(workingbase,settings):
        # check if scancsvfolder provided
        if settings['input'] != None and settings['age'] != None:
            csvlist = scancsvfiles(settings['input'])
            agedict = readagefile(settings['age'])
        else:
            print('The input dataset and/or age file was not defined.')
            print('This is normal if you did not define a input/age parameter.')
            csvlist = []

        # add each csv to the database
        for item in csvlist:
            # Add a subject
            try:
                subject,_ = workingbase.subject(item[0][1],age=agedict[item[0][1]],domhand=item[0][5])
            except KeyError:
                subject,_ = workingbase.subject(item[0][1],age=-1,domhand=item[0][5])

            # Add a visit
            visit,exists = workingbase.visit(subject,item[0][2],date=item[0][3])

            # Only add if not already exists
            if not exists:
                print("Processing {}, Visit {}".format(item[0][1],item[0][2]))
                # load the cv
                visitcsv = CSVdata(item[0][0],item[1][0])

                # Add raw data table to the visit
                raw,_ = workingbase.rawdata(subject,visit,rawobj=visitcsv,rtn_node=True)

if __name__ == "__main__":
    # parse arguments
    parser = argparse.ArgumentParser(description="AnaLysis Of Hemiparetic Accelerometry")
    parser.add_argument('database', help="name of working database")
    parser.add_argument('--input', help="input folder of CSV files")
    parser.add_argument('--age', help="input age file for dataset")
    parser.add_argument('--reset', action="store_true", default=False, help="Reset selected Database")
    parser.add_argument('--cutoff', default=30, help='beginning/ending cutoff in minutes')
    parser.add_argument('--threshold', type=int, default=10, help='set activity threshold (in counts)')
    parser.add_argument('--subject', help='set specific subject to process')
    parser.add_argument('--subjects_only', action="store_true", default=False, help='process subjects only')
    parser.add_argument('--plot', action="store_true", default=False, help="Do plots")
    parser.add_argument('output', help="location of output")

    # convert arguments to dict
    settings = vars(parser.parse_args())

    # reset database if flag enabled
    if settings['reset']:
        try:
            os.remove('{}.h5'.format(settings['database']))
        except FileNotFoundError:
            pass

    # Start a Database
    workingbase = Database(settings['database'])

    # Build Database as needed
    builddatabase(workingbase,settings)

    # Process database
    if settings['output'] != None:
        if os.path.isdir(settings['output']):
            if settings['plot']:
                import matplotlib.pyplot as plt
            workingbase.process(settings)
            workingbase.writecsv(settings)

    # Close database object
    workingbase.close()
