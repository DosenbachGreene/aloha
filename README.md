# AnaLysis Of Hemiparetic Accelerometry

This repo contains **aloha**, a program that sorts and organinzes statistics for Actigraph accelerometry data. There are also other various python scripts for doing other analysis.

## Usage
```
usage: aloha.py [-h] [--input INPUT] [--age AGE] [--reset] [--cutoff CUTOFF]
                [--threshold THRESHOLD] [--subject SUBJECT] [--subjects_only]
                [--plot]
                database output

AnaLysis Of Hemiparetic Accelerometry

positional arguments:
  database              name of working database
  output                location of output

optional arguments:
  -h, --help            show this help message and exit
  --input INPUT         input folder of CSV files
  --age AGE             input age file for dataset
  --reset               Reset selected Database
  --cutoff CUTOFF       beginning/ending cutoff in minutes
  --threshold THRESHOLD
                        set activity threshold (in counts)
  --subject SUBJECT     set specific subject to process
  --subjects_only       process subjects only
  --plot                Do plots
```

## Notes

- age.csv should contain subject name in 1st column, age in 2nd column
- input expects a folder of csv's containing the accelerometry data structured as SUBJECT_V#_MMDDYYYY_HANDEDNESS_DOMINANTHAND.csv e.g. BPEAC008_V1_01012016_L_RH.csv
- database is the name of the database.h5 file (you don't need the extension when typing in)
- cutoff and threshold default to 30 mins and 10 counts by default
