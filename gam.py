#!/usr/bin/env python3

import numpy as np
from pygam import LinearGAM,s,f
import matplotlib.pyplot as plt
import csv

# load data
with open('output/BPEAC_subject_threshold_10.csv','r') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    data = [r for r in reader]

# sort on age
data.sort(key=lambda x: float(x[1]))
Age = [float(r[1]) for r in data]
UseRatio = [float(r[15]) for r in data]
# get domhand
Domhand = [r[2] for r in data]
# get left hand and right hand use hours (avg over 24 hour period)
UseCount = [((float(r[5])/round(float(r[3])/1440))/(60*60),(float(r[6])/round(float(r[3])/1440))/(60*60)) for r in data]
TotalActivity = [time[{'LH': 0,'RH': 1}[hand]] for hand,time in zip(Domhand,UseCount)] # set the total activity to the dominant hand

# load data (hemi)
with open('output/hemiBPEAC_subject_threshold_10.csv','r') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    data = [r for r in reader]

# sort on age
data.sort(key=lambda x: float(x[1]))
hemiAge = [float(r[1]) for r in data]
hemiUseRatio = [float(r[15]) for r in data]
hemiUseCount = [((float(r[5])/round(float(r[3])/1440))/(60*60),(float(r[6])/round(float(r[3])/1440))/(60*60)) for r in data]
# get domhand
hemiDomhand = [r[2] for r in data]
# get left hand and right hand use hours (avg over 24 hour period)
hemiUseCount = [((float(r[5])/round(float(r[3])/1440))/(60*60),(float(r[6])/round(float(r[3])/1440))/(60*60)) for r in data]
hemiTotalActivity = [time[{'LH': 0,'RH': 1}[hand]] for hand,time in zip(hemiDomhand,hemiUseCount)] # set the total activity to the dominant hand

# load data (CIMT)
with open('output/CIMT_subject_threshold_10.csv','r') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    data = [r for r in reader]

# sort on age
data.sort(key=lambda x: float(x[1]))
CIMTAge = [float(r[1]) for r in data]
CIMTUseRatio = [float(r[15]) for r in data]
# get domhand
CIMTDomhand = [r[2] for r in data]
# get left hand and right hand use hours (avg over 24 hour period)
CIMTUseCount = [((float(r[5])/round(float(r[3])/1440))/(60*60),(float(r[6])/round(float(r[3])/1440))/(60*60)) for r in data]
CIMTTotalActivity = [time[{'LH': 0,'RH': 1}[hand]] for hand,time in zip(CIMTDomhand,CIMTUseCount)] # set the total activity to the dominant hand

# create gam model
#gam = LinearGAM(s(0,n_splines=6)).fit(Age,UseRatio)
gam = LinearGAM(s(0)).gridsearch(np.expand_dims(np.array(Age),1),np.array(UseRatio),n_splines=np.arange(1,21))

fig = plt.figure(figsize=(18,6))

# plot UseRatio
ax = fig.add_subplot(1,2,1)
a = ax.scatter(Age,UseRatio,c='g',label='Typical')
b = ax.scatter([8],[0],c='#ffa500',label='Asymmetrical')
ax.plot(Age,gam.predict(Age),c='g')
ax.plot(Age,gam.prediction_intervals(Age,width=0.95),c='grey',ls='--')
ax.set_xlim(0,18)
ax.set_ylim(0.75,1.1)
ax.set_xlabel('Age (years)')
ax.set_ylabel('Use Ratio')
ax.text(17,1.075,'A',fontsize=26)
ax.legend([a,b],['Typical','Asymmetrical'],loc='lower left')

# plot UseRatio
ax = fig.add_subplot(1,2,2)
ax.scatter(Age,UseRatio,c='g')
ax.scatter(hemiAge,hemiUseRatio,c='#ffa500')
ax.scatter(CIMTAge,CIMTUseRatio,c='#ffa500')
ax.plot(Age,gam.predict(Age),c='g')
ax.plot(Age,gam.prediction_intervals(Age,width=0.95),c='grey',ls='--')
ax.set_xlim(0,18)
ax.set_ylim(0.55,1.2)
ax.set_xlabel('Age (years)')
ax.set_ylabel('Use Ratio')
ax.text(17,1.15,'B',fontsize=26)
gam.summary()
plt.savefig('useratio.eps')

# create gam model
#gam2 = LinearGAM(s(0,n_splines=6)).fit(Age,TotalActivity)
gam2 = LinearGAM(s(0)).gridsearch(np.expand_dims(np.array(Age),1),np.array(TotalActivity),n_splines=np.arange(1,21))

# plot TotalActivity
plt.figure(figsize=(9,6))
plt.scatter(Age,TotalActivity,c='g',label='Typical')
plt.scatter(hemiAge,hemiTotalActivity,c='#ffa500',label='Asymmetrical')
plt.scatter(CIMTAge,CIMTTotalActivity,c='#ffa500')
plt.plot(Age,gam2.predict(Age),c='g')
plt.plot(Age,gam2.prediction_intervals(Age,width=0.95),c='grey',ls='--')
plt.xlim(0,18)
plt.ylim(5,12)
plt.xlabel('Age (years)')
plt.ylabel('Total Activity (hours)')
plt.legend(loc='lower left')
gam.summary()
plt.savefig('totalactivity.eps')

plt.show()
