#!/usr/bin/env python3

from sklearn import linear_model
from sklearn import metrics
import csv
import numpy as np
from matplotlib import pyplot as plt
from sklearn.model_selection import GridSearchCV, cross_validate, StratifiedKFold
from scipy.stats import mode
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
np.random.seed(1000)

NUM_TRIALS = 30
threshold = 10
res = "subject" # resolution
xaxis = "MAUI"
yaxis = "BAUI"

# open csv
BPEAC = []
hemiBPEAC = []
CIMT = []
with open('./output/BPEAC_{}_threshold_{}.csv'.format(res,threshold),'r',newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        BPEAC.append(row)
with open('./output/hemiBPEAC_{}_threshold_{}.csv'.format(res,threshold),'r',newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        hemiBPEAC.append(row)
with open('./output/CIMT_{}_threshold_{}.csv'.format(res,threshold),'r',newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        CIMT.append(row)

# takeout data
takeout = ['BPEAC013H','BPEAC229H','BPEAC254H']
#takeout = []
hemiBPEAC = [sub for sub in hemiBPEAC if sub['Subject'] not in takeout]
CIMT = [sub for sub in CIMT if sub['Subject'] not in takeout]

# create a np array out of the data
BPEAC_x = np.asarray([visit[xaxis] for visit in BPEAC],dtype=float)
BPEAC_y = np.asarray([visit[yaxis] for visit in BPEAC],dtype=float)
hemiBPEAC_x = np.asarray([visit[xaxis] for visit in hemiBPEAC],dtype=float)
hemiBPEAC_y = np.asarray([visit[yaxis] for visit in hemiBPEAC],dtype=float)
CIMT_x = np.asarray([visit[xaxis] for visit in CIMT],dtype=float)
CIMT_y = np.asarray([visit[yaxis] for visit in CIMT],dtype=float)
hemi_x = np.concatenate((hemiBPEAC_x,CIMT_x))
hemi_y = np.concatenate((hemiBPEAC_y,CIMT_y))

# concatenate arrays and define outputs
x_cat = np.concatenate((BPEAC_x,hemi_x)).reshape(-1,1)
y_cat = np.concatenate((BPEAC_y,hemi_y)).reshape(-1,1)
data = np.column_stack((x_cat,y_cat))
targets  = np.concatenate((np.zeros(len(BPEAC)),np.ones(len(hemiBPEAC)),np.ones(len(CIMT))))

# Create potential values for C
p_grid = {"C": np.logspace(-7,2,10)}

# setup logistic regression classifier
logreg = linear_model.LogisticRegression(solver='liblinear')

# Loop for each trial
Reg = list(); Stats = {'accuracy': [], 'f1_score': [], 'precision': [], 'recall': [], 'AUC': []}
for i in range(NUM_TRIALS):
    print("Trial {}".format(i))
    # Choose cross-validation techniques for the inner and outer loops,
    # independently of the dataset.
    inner_cv = StratifiedKFold(n_splits=7, shuffle=True, random_state=i)
    outer_cv = StratifiedKFold(n_splits=7, shuffle=True, random_state=i)

    # Parameter Search
    clf = GridSearchCV(estimator=logreg, param_grid=p_grid, cv=inner_cv)
    clf.fit(data,targets)
    Reg.append(clf.best_params_['C'])

    # Run cross validation for estimating model performance
    cv_out = cross_validate(clf, X=data, y=targets, cv=outer_cv, return_estimator=True)
    Stats['accuracy'].append(cv_out['test_score'].mean())

    # For each estimator in cross validation fold, find F1, Precision, Recall, AUC
    f1_score = list(); precision = list(); recall = list(); AUC = list()
    outer_cv2 = StratifiedKFold(n_splits=7, shuffle=True, random_state=i) # create another split we can look over
    for estimator,(_,test) in zip(cv_out['estimator'],outer_cv2.split(data,targets)):
        # get probailities on test set
        prob = estimator.predict_proba(data[test,:])[:,1]
        # get classifications
        classified = (prob > 0.5).astype('f8')

        # get metrics
        f1_score.append(metrics.f1_score(targets[test],classified))
        precision.append(metrics.precision_score(targets[test],classified))
        recall.append(metrics.recall_score(targets[test],classified))
        dec = estimator.decision_function(data[test,:])
        fpr,tpr,thresh = metrics.roc_curve(targets[test],dec,pos_label=1)
        AUC.append(metrics.auc(fpr,tpr))

    # Get means and append
    Stats['f1_score'].append(np.array(f1_score).mean())
    Stats['precision'].append(np.array(precision).mean())
    Stats['recall'].append(np.array(recall).mean())
    Stats['AUC'].append(np.array(AUC).mean())

# get most often used regularization term
C = mode(np.array(Reg)).mode[0]

# convert to numpy arrays and print
for key in Stats:
    Stats[key] = np.array(Stats[key])
    print('{}: mean = {}, std = {}'.format(key,Stats[key].mean(),Stats[key].std()))

# fit the logistic Logistic Regression
logreg = linear_model.LogisticRegression(C=C, solver='liblinear')
logreg.fit(data,targets)

# create figure for plotting
fig = plt.figure(figsize=(20,6))
ax = fig.add_subplot(1,2,1)

# plot boundary
w = logreg.coef_[0]
xx = np.arange(0,2,0.01)
yy = (-w[0]*xx/w[1]) - (logreg.intercept_[0]/w[1])
plt.plot(xx,yy,'k-')

# plot contour
xx,yy = np.mgrid[0:2:0.01,0:2:0.01]
grid = np.c_[xx.ravel(),yy.ravel()]
probs = logreg.predict_proba(grid)[:,1].reshape(xx.shape)
contour = ax.contourf(xx,yy,probs,500,cmap="Purples",vmin=0,vmax=1)
#ax_c = fig.colorbar(contour)
#ax_c.set_label("Probability of Hemi Classification")
#ax_c.set_ticks([0, .25, .5, .75, 1])

# plot the data
ax.scatter(BPEAC_x,BPEAC_y,c='green',label='Typical',alpha=0.8, edgecolors='none')
ax.scatter(hemi_x,hemi_y,c='#ffa500',label='Asymmetrical',alpha=0.8, edgecolors='none')
ax.set_xlim([0,2])
ax.set_xlabel(xaxis)
ax.set_ylim([0.4,1.3])
ax.set_ylabel(yaxis)
ax.legend(loc=4)
plt.savefig('logreg_maui_baui.eps')

import pickle
with open('test.pkl','wb') as f:
    pickle.dump(fig,f)

# show plots
plt.show()

# accuracy: mean = 0.9781779948446616, std = 0.00015483261715522708
# f1_score: mean = 0.9162433862433861, std = 0.007564260902951066
# precision: mean = 1.0, std = 0.0
# recall: mean = 0.8626190476190475, std = 0.0054346248621491835
# AUC: mean = 0.983917748917749, std = 0.0038559741890291607
